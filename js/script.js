async function fetchData() {
    document.getElementById('loading').style.display = 'block';
    
    await new Promise(resolve => setTimeout(resolve, 1000));
    
    const usersResponse = await fetch('https://ajax.test-danit.com/api/json/users');
    const postsResponse = await fetch('https://ajax.test-danit.com/api/json/posts');
    
    const users = await usersResponse.json();
    const posts = await postsResponse.json();
    
    document.getElementById('loading').style.display = 'none';
    return { users, posts };
}

class Card {
    constructor(post, user) {
        this.post = post;
        this.user = user;
    }

    createCardElement() {
        const card = document.createElement('div');
        card.classList.add('card');

        const title = document.createElement('h2');
        title.textContent = this.post.title;

        const body = document.createElement('p');
        body.textContent = this.post.body;

        const userInfo = document.createElement('div');
        userInfo.classList.add('user-info');
        userInfo.textContent = `${this.user.name} ${this.user.email}`;

        const editBtn = document.createElement('button');
        editBtn.classList.add('edit-btn');
        editBtn.textContent = 'Edit';
        editBtn.addEventListener('click', () => {
            this.editPost(card);
        });

        const deleteBtn = document.createElement('button');
        deleteBtn.classList.add('delete-btn');
        deleteBtn.textContent = 'Delete';
        deleteBtn.addEventListener('click', async () => {
            await this.deletePost();
            card.remove();
        });

        card.append(title, body, userInfo, editBtn, deleteBtn);
        return card;
    }

    async deletePost() {
        const response = await fetch(`https://ajax.test-danit.com/api/json/posts/${this.post.id}`, {
            method: 'DELETE',
        });

        if (!response.ok) {
            alert('Failed to delete post');
        }
    }

    editPost(card) {
        const modal = document.getElementById('edit-modal');
        const editTitleInput = document.getElementById('edit-title-input');
        const editBodyInput = document.getElementById('edit-body-input');
        const submitEditBtn = document.getElementById('submit-edit-btn');

        editTitleInput.value = this.post.title;
        editBodyInput.value = this.post.body;

        modal.style.display = 'block';

        submitEditBtn.onclick = async () => {
            this.post.title = editTitleInput.value;
            this.post.body = editBodyInput.value;

            const response = await fetch(`https://ajax.test-danit.com/api/json/posts/${this.post.id}`, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(this.post),
            });

            if (response.ok) {
                card.querySelector('h2').textContent = this.post.title;
                card.querySelector('p').textContent = this.post.body;
                modal.style.display = 'none';
            } else {
                alert('Failed to edit post');
            }
        };
    }
}

async function init() {
    const { users, posts } = await fetchData();
    const feed = document.getElementById('feed');

    posts.forEach(post => {
        const user = users.find(user => user.id === post.userId);
        const card = new Card(post, user);
        feed.appendChild(card.createCardElement());
    });
}

init();

const modal = document.getElementById('modal');
const addPostBtn = document.getElementById('add-post-btn');
const closeBtn = document.getElementsByClassName('close')[0];
const submitPostBtn = document.getElementById('submit-post-btn');

addPostBtn.onclick = () => {
    modal.style.display = 'block';
};

closeBtn.onclick = () => {
    modal.style.display = 'none';
};

window.onclick = (event) => {
    if (event.target == modal) {
        modal.style.display = 'none';
    }
};

submitPostBtn.onclick = async () => {
    const titleInput = document.getElementById('title-input').value;
    const bodyInput = document.getElementById('body-input').value;

    const newPost = {
        userId: 1,
        title: titleInput,
        body: bodyInput,
    };

    const response = await fetch('https://ajax.test-danit.com/api/json/posts', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(newPost),
    });

    if (response.ok) {
        const post = await response.json();
        const user = { id: 1, name: 'User Name', email: 'user@example.com' };
        const card = new Card(post, user);
        document.getElementById('feed').prepend(card.createCardElement());
        modal.style.display = 'none';
    } else {
        alert('Failed to add post');
    }
};

const editModal = document.getElementById('edit-modal');
const editCloseBtn = document.getElementsByClassName('edit-close')[0];

editCloseBtn.onclick = () => {
    editModal.style.display = 'none';
};

window.onclick = (event) => {
    if (event.target == editModal) {
        editModal.style.display = 'none';
    }
};





